import Sequelize from 'sequelize'
import path from 'path'
import { remote } from 'electron'

let databasePath = path.join(remote.app.getPath('userData'), '/db.sqlite3')

export const connection = new Sequelize({
  dialect: 'sqlite',
  storage: databasePath,
  operatorsAliases: false
})

export const TaskModel = connection.define('task', {
  date: {
    type: Sequelize.DATEONLY,
    allowNull: false
  },
  task: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notEmpty: true
    }
  },
  description: {
    type: Sequelize.TEXT
  },
  done: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false
  }
})
