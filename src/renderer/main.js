import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import { connection, TaskModel } from './database'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faChevronRight, faChevronLeft, faCalendarAlt, faEllipsisV, faUndoAlt, faCircle, faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import MaterializeCSS from 'materialize-css'

connection.sync()

Vue.use(MaterializeCSS)
library.add(faChevronRight, faChevronLeft, faCalendarAlt, faEllipsisV, faUndoAlt, faCircle, faCheckCircle)
Vue.component('font-awesome-icon', FontAwesomeIcon)

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.config.productionTip = false

Vue.prototype.$db = connection
Vue.prototype.$models = { TaskModel }

Vue.mixin({
  methods: {
    goTo (date) {
      this.$router.replace({
        name: 'singleDay',
        params: {
          day: date.format('DD'),
          month: date.format('MM'),
          year: date.year()
        }
      })
    }
  }
})

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
